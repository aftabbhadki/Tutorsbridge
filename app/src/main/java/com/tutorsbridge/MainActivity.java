package com.tutorsbridge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.tutorsbridge.Adapters.TutorListAdapter;
import com.tutorsbridge.AsynctaskTutorbridge.Asynctasktotur;
import com.tutorsbridge.Models.TutorModel;
import com.tutorsbridge.Network.InternetConnection;



public class MainActivity extends AppCompatActivity {
    TextView tv_internet;

    RecyclerView recyclerView;
    TutorListAdapter tutorListAdapter;
    List<TutorModel> list = new ArrayList<>();
    private final static String JSON_URL =
            "http://mdevtbridgeservices.tutorsbridge.com/BridgeAPI/jobs/GetTeachingJobs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.rv_list);
        tv_internet = (TextView) findViewById(R.id.tv_internet);

       /*list.add( new TutorModel("Chiru","4.2 years experience",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Chiru","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Chiru","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Mahaevi","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Aftab","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Mahaevi","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Aftab","4.2 years experience in Android",
                "Android","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Mahaevi","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Aftab","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Mahaevi","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));
        list.add( new TutorModel("Aftab","4.2 years experience in Android",
                "10th Class Maths","7795087286","Bangalore","Bangalore"));*/

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new TutorListAdapter(this,list));

        if(InternetConnection.checkConnection(MainActivity.this))
        new Asynctasktotur(this, JSON_URL).execute();
        else{
            tv_internet.setVisibility(View.VISIBLE);
            //Toast.makeText(MainActivity.this,"No Internet",Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    protected void onStart() {
        super.onStart();

        //Load JSON data from an URL

    }

    public void parseJsonResponse(String result) {
        try {


            Log.d("dddd", ""+result) ;

            JSONArray array = new JSONArray(result) ;



            list.clear();
            for (int i = 0; i < array.length(); i++) {

                JSONObject jObject = array.getJSONObject(i);
                TutorModel country = new TutorModel() ;
                country.setName(jObject.getString("ContactName"));
                country.setSubject(jObject.getString("Subject"));
                country.setExperience(jObject.getString("Experience"));
                country.setContactnumber(jObject.getString("ContactNos"));
                country.setLocation(jObject.getString("Location"));
                country.setCity(jObject.getString("City"));
                country.setLandmark(jObject.getString("Landmark"));

                list.add(country);
              //  Log.d("dddddd",""+country.getSubject()) ;
            }



            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new TutorListAdapter(this,list));



           // tutorListAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
