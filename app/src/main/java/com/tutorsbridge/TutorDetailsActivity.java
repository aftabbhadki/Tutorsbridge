package com.tutorsbridge;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


public class TutorDetailsActivity extends AppCompatActivity implements View.OnClickListener{
    TextView tv_name, tv_experience, tv_suject, tv_address, tv_mobilenumber;
    TextView main_title;
    Button action_intrested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_details);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_experience = (TextView) findViewById(R.id.tv_experience);
        tv_suject = (TextView) findViewById(R.id.tv_suject);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_mobilenumber = (TextView) findViewById(R.id.tv_mobilenumber);
        main_title = (TextView) findViewById(R.id.main_title);
        main_title.setText("Tutor Details");
        initview();

        Intent intent = getIntent();
        if (intent != null) {
            tv_name.setText("Name: " +intent.getStringExtra( "name"));
            tv_experience.setText("Experience: " +intent.getStringExtra( "experiance"));
            tv_suject.setText("Subjects: " + intent.getStringExtra( "subject"));
            tv_address.setText(intent.getStringExtra("location") +System.getProperty("line.separator")+
                    intent.getStringExtra("landmark") + System.getProperty("line.separator")
                    +intent.getStringExtra("city"));
            String mobile = intent.getStringExtra("mobilenummber");
            String[] parts = mobile.split(",");
            String mobile1 = parts[0];
            String mobile2 = parts[1];
            tv_mobilenumber.setText(mobile1 + System.getProperty("line.separator") + mobile2);

        }


    }

    private void initview() {
        action_intrested = (Button)findViewById(R.id.action_intrested);
        action_intrested.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_intrested:
                showalertdialog();


                break;
        }

    }

    private void showalertdialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(TutorDetailsActivity.this);
        dialog.setTitle("Add Details");
        LinearLayout layout = new LinearLayout(TutorDetailsActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);


// Add a TextView here for the "Title" label, as noted in the comments
        final EditText titleBox = new EditText(TutorDetailsActivity.this);

        titleBox.setHint("Name");
       // titleBox.setLayoutParams(new LinearLayout.LayoutParams(ma));
        layout.addView(titleBox); // Notice this is an add method

// Add another TextView here for the "Description" label
        final EditText descriptionBox = new EditText(TutorDetailsActivity.this);

        descriptionBox.setHint("Email Address");
        layout.addView(descriptionBox); // Another add method

        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setView(layout);
        dialog.setCancelable(false);
        dialog.show();
    }
}
