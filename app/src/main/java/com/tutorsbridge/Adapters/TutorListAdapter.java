package com.tutorsbridge.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.tutorsbridge.Models.TutorModel;

import com.tutorsbridge.R;
import com.tutorsbridge.TutorDetailsActivity;

/**
 * Created by test on 20/01/18.
 */

public class TutorListAdapter extends RecyclerView.Adapter<TutorListAdapter.MyViewHolder> {

    private List<TutorModel> imagesModelListList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, experiece, subject;
        public TextView action_more;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            experiece = (TextView) view.findViewById(R.id.tv_experience);
            subject = (TextView) view.findViewById(R.id.tv_suject);
            action_more = (TextView) view.findViewById(R.id.action_more);

        }
    }


    public TutorListAdapter(Context context, List<TutorModel> imagesModelListList) {
        this.imagesModelListList = imagesModelListList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TutorModel images = imagesModelListList.get(position);
        holder.name.setText("Name: "+images.getName());
        holder.experiece.setText("Experience: "+images.getExperience());
        holder.subject.setText("Subject: "+images.getSubject());
        holder.action_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(context,TutorDetailsActivity.class);

                intent.putExtra("name",images.getName()) ;
                intent.putExtra("subject",images.getSubject()) ;
                intent.putExtra("experiance",images.getExperience()) ;
                intent.putExtra("location",images.getLocation()) ;
                intent.putExtra("landmark", images.getLandmark()) ;
                intent.putExtra("city",images.getCity()) ;
                intent.putExtra("mobilenummber",images.getContactnumber());

                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return imagesModelListList.size();
    }
}
