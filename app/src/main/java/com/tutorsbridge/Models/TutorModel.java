package com.tutorsbridge.Models;

/**
 * Created by mahadevi on 17-03-2018.
 */

public class TutorModel {
    private String name,experience,subject,contactnumber,location,city, landmark;

    public TutorModel(String name, String experience, String subject, String contactnumber, String location, String city,String landmark) {
        this.name = name;
        this.experience = experience;
        this.subject = subject;
        this.contactnumber = contactnumber;
        this.location = location;
        this.city = city;
        this.landmark = landmark ;
    }

    public  TutorModel()
    {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }
}
